#ВАЖНО! Запускать требуется из монорепозитория
FROM openjdk:11

#Создаем папку для библиотеки классов
RUN mkdir /DataTransferObjectLib

#Создаем папку для севиса
RUN mkdir /gateway-service

#Копируем библиотеку классов в образ
COPY ./DataTransferObjectLib /DataTransferObjectLib/

#Копируем сервис
COPY ./gateway-service /gateway-service/

#Устанавливаем рабочую дирректорию
WORKDIR /gateway-service

#Собираем проект
RUN  ./gradlew clean && ./gradlew build -x test

#Сразу прокидываем нужный порт
EXPOSE 4000

#Создаем дирректорию для запуска
RUN mkdir /app

#Копируем JAR в дирректорию для запуска
RUN cp /gateway-service/build/libs/*.jar /app/gateway-service.jar

#запуск сервиса
ENTRYPOINT [\
"java",\
"-XX:+UnlockExperimentalVMOptions",\
"-Djava.security.egd=file:/dev/./urandom",\
"-jar","/app/gateway-service.jar",\
"--spring.config.location=file:///gateway-service/src/main/resources/production.yml"]