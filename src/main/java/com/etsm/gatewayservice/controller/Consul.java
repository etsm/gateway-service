package com.etsm.gatewayservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/consul")
@RestController
public class Consul {

    @GetMapping("/ruok")
    public String ruok(){
        return "imok";
    }
}
