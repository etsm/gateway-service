package com.etsm.gatewayservice.datafetchers.mutation;

import DTO.*;
import com.etsm.gatewayservice.interfaces.authService.AuthServiceProxy;
import com.etsm.gatewayservice.interfaces.employeeService.mutation.EmployeeMutationProxy;
import com.etsm.gatewayservice.services.securityChecker.SecurityChecker;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/*
  mutation{
    employee{
      createEmployee(login: "usr", firstName: "Anton", secondName: "quit", email: "q@q.com", accessToSystem: true){
        id
        firstName
        secondName
        accessRoleSet{
          name
        }
      }
    }
  }


mutation{
    employee{
      updateEmployee(id:4, accessRoleIds: [1], login: "usr", firstName: "Anton", secondName: "quit", email: "q@q.com", accessToSystem: true){
        id
        firstName
        secondName
        accessRoleSet{
          name
        }
      }
    }
}

* */

@DgsComponent
@Component
public class EmployeeMutation {
    private final EmployeeMutationProxy employeeMutationProxy;
    private final SecurityChecker securityChecker;
    private final AuthServiceProxy authServiceProxy;

    public EmployeeMutation(
            EmployeeMutationProxy employeeMutationProxy,
            SecurityChecker securityChecker,
            AuthServiceProxy authServiceProxy) {
        this.employeeMutationProxy = employeeMutationProxy;
        this.securityChecker = securityChecker;
        this.authServiceProxy = authServiceProxy;
    }

    @DgsData(field = "employee", parentType = "Mutation")
    public DataFetchingEnvironment wrapper(DataFetchingEnvironment dfe, @RequestHeader String token) {
      return this.securityChecker.isAuthorized(dfe, token);
    }

    @DgsData(field = "createEmployee", parentType = "employee_mutation")
    public EmployeeDTO createEmployee(
            @InputArgument String login,
            @InputArgument String firstName,
            @InputArgument String secondName,
            @InputArgument String patronymic,
            @InputArgument Boolean accessToSystem,
            @InputArgument String email,
            @InputArgument Long departmentId,
            @InputArgument List<Long> accessRoleIds
    ) throws Exception {
        DepartmentDTO department = null;

        if(departmentId != null){
            department = new DepartmentDTO();
            department.setId(departmentId);
        }


        Set<AccessRoleDTO> accessRoleDTOSet = null;
        if(accessRoleIds != null){
            accessRoleDTOSet = accessRoleIds.stream().map(id -> {
                AccessRoleDTO accessRoleDTO = new AccessRoleDTO();
                accessRoleDTO.setId(id);
                return accessRoleDTO;
            }).collect(Collectors.toSet());
        }


        LoginDTO loginDTO = (LoginDTO) LoginDTO.builder()
                .login(login)
                .firstName(firstName)
                .secondName(secondName)
                .patronymic(patronymic)
                .accessToSystem(accessToSystem)
                .email(email)
                .department(department)
                .accessRoleSet(accessRoleDTOSet)
                .build();

        return this.employeeMutationProxy.createEmployee(loginDTO);
    }

    @DgsData(field = "updateEmployee", parentType = "employee_mutation")
    public EmployeeDTO updateEmployee(
            @InputArgument Long id,
            @InputArgument String login,
            @InputArgument String firstName,
            @InputArgument String secondName,
            @InputArgument String patronymic,
            @InputArgument Boolean accessToSystem,
            @InputArgument String email,
            @InputArgument Long departmentId,
            @InputArgument List<Long> accessRoleIds
    ) throws Exception {
        DepartmentDTO department = null;

        if(departmentId != null){
            department = new DepartmentDTO();
            department.setId(departmentId);
        }
        Set<AccessRoleDTO> accessRoleDTOSet = null;
        if(accessRoleIds != null){
            accessRoleDTOSet = accessRoleIds.stream().map(roleId -> {
                AccessRoleDTO accessRoleDTO = new AccessRoleDTO();
                accessRoleDTO.setId(roleId);
                return accessRoleDTO;
            }).collect(Collectors.toSet());
        }


        LoginDTO loginDTO = (LoginDTO) LoginDTO.builder()
                .id(id)
                .login(login)
                .firstName(firstName)
                .secondName(secondName)
                .patronymic(patronymic)
                .accessToSystem(accessToSystem)
                .email(email)
                .department(department)
                .accessRole(accessRoleDTOSet)
                .build();

        return this.employeeMutationProxy.updateEmployeeById(loginDTO);
    }

    @DgsData(field = "remove", parentType = "employee_mutation")
    public Boolean remove(@InputArgument List<Long> ids){
        ListIdsDTO listIdsDTO = new ListIdsDTO(ids);

        return this.employeeMutationProxy.remove(listIdsDTO);
    }

    @DgsData(field = "logout", parentType = "employee_mutation")
    public Boolean logout(@NotNull @RequestHeader String token){
        Credentials credentials = new Credentials();
        credentials.setToken(token);

        return this.authServiceProxy.logout(credentials);
    }
}
