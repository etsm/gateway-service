package com.etsm.gatewayservice.datafetchers.mutation;

import DTO.AccessRoleDTO;
import DTO.EmployeeDTO;
import DTO.ListIdsDTO;
import com.etsm.gatewayservice.interfaces.employeeService.mutation.AccessRoleMutationProxy;
import com.etsm.gatewayservice.services.securityChecker.SecurityChecker;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import exceptions.NotFoundException;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@DgsComponent
public class AccessRoleMutation {

    private final AccessRoleMutationProxy accessRoleMutationProxy;
    private final SecurityChecker securityChecker;

    public AccessRoleMutation(AccessRoleMutationProxy accessRoleMutationProxy, SecurityChecker securityChecker) {
        this.accessRoleMutationProxy = accessRoleMutationProxy;
        this.securityChecker = securityChecker;
    }

    @DgsData(field = "accessRole", parentType = "Mutation")
    public DataFetchingEnvironment wrapper(DataFetchingEnvironment dfe, @RequestHeader String token) {
        return this.securityChecker.isAuthorized(dfe, token);
    }

    @DgsData(field = "createAccessRole", parentType = "access_role_mutation")
    public AccessRoleDTO createAccessRole(@InputArgument String name){
        AccessRoleDTO accessRoleDTO = AccessRoleDTO.builder().name(name).build();

        return this.accessRoleMutationProxy.createAccessRole(accessRoleDTO);
    }

    @DgsData(field = "updateAccessRole", parentType = "access_role_mutation")
    public AccessRoleDTO updateAccessRole(@InputArgument Long id, @InputArgument String name, @InputArgument List<Long> employeeIds) throws NotFoundException {
        Set<EmployeeDTO> employeeDTOSet = null;
        if(employeeIds != null){
            employeeDTOSet = employeeIds.stream()
                    .map(employeeId -> EmployeeDTO.builder().id(employeeId).build())
                    .collect(Collectors.toSet());
        }

        AccessRoleDTO accessRoleDTO = AccessRoleDTO.builder().id(id).name(name).employees(employeeDTOSet).build();

        return this.accessRoleMutationProxy.updateAccessRole(accessRoleDTO);
    }

    @DgsData(field = "remove", parentType = "access_role_mutation")
    public Boolean remove(@InputArgument List<Long> ids){
        ListIdsDTO listIdsDTO = new ListIdsDTO(ids);

        return this.accessRoleMutationProxy.remove(listIdsDTO);
    }

}
