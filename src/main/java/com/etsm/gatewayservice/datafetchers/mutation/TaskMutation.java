package com.etsm.gatewayservice.datafetchers.mutation;

import DTO.ListIdsDTO;
import DTO.TaskDTO;
import DTO.WorkspaceDTO;
import builders.TaskDTOBuilder;
import com.etsm.gatewayservice.interfaces.taskService.mutation.TaskServiceMutationProxy;
import com.etsm.gatewayservice.services.securityChecker.SecurityChecker;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.web.bind.annotation.RequestHeader;
import utils.ETaskStatus;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

/*

mutation{
  task{
    createTask(title:"Second task" status: READY_FOR_WORK, workspaceId:1, createTime: "2021-06-08T23:42:00"){
      id
      taskIdentifier
      title
      workspace{
        id
        name
        taskSet{
          id
          title
        }
      }
    }
  }
}
 */

@DgsComponent
public class TaskMutation {

    private final TaskServiceMutationProxy taskServiceMutationProxy;
    private final SecurityChecker securityChecker;

    public TaskMutation(TaskServiceMutationProxy taskServiceMutationProxy, SecurityChecker securityChecker) {
        this.taskServiceMutationProxy = taskServiceMutationProxy;
        this.securityChecker = securityChecker;
    }

    @DgsData(field = "task", parentType = "Mutation")
    public DataFetchingEnvironment wrapper(DataFetchingEnvironment dfe, @RequestHeader String token) {
        return this.securityChecker.isAuthorized(dfe, token);
    }

    @DgsData(field = "createTask", parentType = "task_mutation")
    public TaskDTO createTask(
            @InputArgument String title,
            @InputArgument ETaskStatus status,
            @InputArgument Long workspaceId,
            @InputArgument LocalDateTime createTime,
            @InputArgument Long executorId,
            @InputArgument Long authorId,
            @InputArgument String description,
            @InputArgument LocalDateTime startTime,
            @InputArgument LocalDateTime endTime,
            @InputArgument Long initialEstimateOfTaskExecutionTime,
            @InputArgument Long elapsedTaskExecutionTime,
            @InputArgument Long timeRemainingForTheTask
    ) throws Exception {
        TaskDTOBuilder taskDTOBuilder = TaskDTO.builder()
                .title(title)
                .status(status)
                .workspace(WorkspaceDTO.builder().id(workspaceId).build())
                .createTime(createTime)
                .executorId(executorId)
                .authorId(authorId)
                .description(description)
                .startTime(startTime)
                .endTime(endTime);

        if(initialEstimateOfTaskExecutionTime != null){
            taskDTOBuilder.initialEstimateOfTaskExecutionTime(Duration.ofMinutes(initialEstimateOfTaskExecutionTime));
        }

        if(elapsedTaskExecutionTime != null){
            taskDTOBuilder.elapsedTaskExecutionTime(Duration.ofMinutes(elapsedTaskExecutionTime));
        }

        if(timeRemainingForTheTask != null){
            taskDTOBuilder.timeRemainingForTheTask(Duration.ofMinutes(timeRemainingForTheTask));
        }

        TaskDTO taskDTO = taskDTOBuilder.build();

        return this.taskServiceMutationProxy.createTask(taskDTO);
    }

    @DgsData(field = "updateTask", parentType = "task_mutation")
    public TaskDTO updateTask(
            @InputArgument Long id,
            @InputArgument String title,
            @InputArgument ETaskStatus status,
            @InputArgument Long workspaceId,
            @InputArgument LocalDateTime createTime,
            @InputArgument Long executorId,
            @InputArgument Long authorId,
            @InputArgument String description,
            @InputArgument LocalDateTime startTime,
            @InputArgument LocalDateTime endTime,
            @InputArgument Long initialEstimateOfTaskExecutionTime,
            @InputArgument Long elapsedTaskExecutionTime,
            @InputArgument Long timeRemainingForTheTask
    ) throws Exception {
        TaskDTO taskDTO = TaskDTO.builder()
                .id(id)
                .title(title)
                .status(status)
                .workspace(WorkspaceDTO.builder().id(workspaceId).build())
                .createTime(createTime)
                .executorId(executorId)
                .authorId(authorId)
                .description(description)
                .startTime(startTime)
                .endTime(endTime)
                .initialEstimateOfTaskExecutionTime(Duration.ofMinutes(initialEstimateOfTaskExecutionTime))
                .elapsedTaskExecutionTime(Duration.ofMinutes(elapsedTaskExecutionTime))
                .timeRemainingForTheTask(Duration.ofMinutes(timeRemainingForTheTask))
                .build();

        return this.taskServiceMutationProxy.updateTask(taskDTO);
    }

    @DgsData(field = "updateBlocksTaskList", parentType = "task_mutation")
    public TaskDTO updateBlocksTaskList(
            @InputArgument Long taskId,
            @InputArgument List<Long> ids
    ) throws Exception {
        return this.taskServiceMutationProxy.updateBlocksListTask(taskId.toString(), new ListIdsDTO(ids));
    }

    @DgsData(field = "updateBlockedTaskList", parentType = "task_mutation")
    public TaskDTO updateBlockedTaskList(
            @InputArgument Long taskId,
            @InputArgument List<Long> ids
    ) throws Exception {
        return this.taskServiceMutationProxy.updateBlockedListTask(taskId.toString(), new ListIdsDTO(ids));
    }
}
