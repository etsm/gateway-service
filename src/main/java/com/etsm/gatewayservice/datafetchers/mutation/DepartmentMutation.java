package com.etsm.gatewayservice.datafetchers.mutation;

import DTO.DepartmentDTO;
import DTO.EmployeeDTO;
import DTO.ListIdsDTO;
import com.etsm.gatewayservice.interfaces.employeeService.mutation.DepartmentMutationProxy;
import com.etsm.gatewayservice.services.securityChecker.SecurityChecker;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import exceptions.NotFoundException;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@DgsComponent
public class DepartmentMutation {

    private final DepartmentMutationProxy departmentMutationProxy;
    private final SecurityChecker securityChecker;

    public DepartmentMutation(DepartmentMutationProxy departmentMutationProxy, SecurityChecker securityChecker) {
        this.departmentMutationProxy = departmentMutationProxy;
        this.securityChecker = securityChecker;
    }

    @DgsData(field = "department", parentType = "Mutation")
    public DataFetchingEnvironment wrapper(DataFetchingEnvironment dfe, @RequestHeader String token) {
        return this.securityChecker.isAuthorized(dfe, token);
    }

    @DgsData(field = "createDepartment", parentType = "department_mutation")
    public DepartmentDTO createDepartment(@InputArgument String name, @InputArgument Long headOfDepartmentId) throws NotFoundException {
        EmployeeDTO headOfDepartment = null;

        if(headOfDepartmentId != null){
            headOfDepartment = EmployeeDTO.builder().id(headOfDepartmentId).build();
        }

        return this.departmentMutationProxy.createDepartment(DepartmentDTO.builder().name(name).headOfDepartment(headOfDepartment).build());
    }

    @DgsData(field = "updateDepartment", parentType = "department_mutation")
    public DepartmentDTO updateDepartment(@InputArgument Long id, @InputArgument String name, @InputArgument Long headOfDepartmentId) throws NotFoundException {
        EmployeeDTO headOfDepartment = null;

        if(headOfDepartmentId != null){
            headOfDepartment = EmployeeDTO.builder().id(headOfDepartmentId).build();
        }

        return this.departmentMutationProxy.updateDepartment(DepartmentDTO.builder()
                .id(id)
                .name(name)
                .headOfDepartment(headOfDepartment)
                .build());
    }

    @DgsData(field = "remove", parentType = "department_mutation")
    public Boolean remove(@InputArgument List<Long> ids){
        ListIdsDTO listIdsDTO = new ListIdsDTO(ids);

        return this.departmentMutationProxy.remove(listIdsDTO);
    }
}
