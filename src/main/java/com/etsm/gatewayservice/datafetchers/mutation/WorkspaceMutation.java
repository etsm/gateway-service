package com.etsm.gatewayservice.datafetchers.mutation;

import DTO.ListIdsDTO;
import DTO.WorkspaceDTO;
import com.etsm.gatewayservice.interfaces.taskService.mutation.WorkspaceMutationProxy;
import com.etsm.gatewayservice.services.securityChecker.SecurityChecker;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import exceptions.NotFoundException;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@DgsComponent
public class WorkspaceMutation {

    private final WorkspaceMutationProxy workspaceMutationProxy;
    private final SecurityChecker securityChecker;

    public WorkspaceMutation(WorkspaceMutationProxy workspaceMutationProxy, SecurityChecker securityChecker) {
        this.workspaceMutationProxy = workspaceMutationProxy;
        this.securityChecker = securityChecker;
    }

    @DgsData(field = "workspace", parentType = "Mutation")
    public DataFetchingEnvironment wrapper(DataFetchingEnvironment dfe, @RequestHeader String token) {
        return this.securityChecker.isAuthorized(dfe, token);
    }

    @DgsData(field = "createWorkspace", parentType = "workspace_mutation")
    public WorkspaceDTO createWorkspace(
            @InputArgument Long departmentId,
            @InputArgument String identifier,
            @InputArgument String name,
            @InputArgument String description
    ) {
        WorkspaceDTO workspaceDTO = WorkspaceDTO.builder()
                .departmentId(departmentId)
                .identifier(identifier)
                .name(name)
                .description(description)
                .build();

        return this.workspaceMutationProxy.createWorkspace(workspaceDTO);
    }

    @DgsData(field = "updateWorkspace", parentType = "workspace_mutation")
    public WorkspaceDTO updateWorkspace(
            @InputArgument Long id,
            @InputArgument Long departmentId,
            @InputArgument String identifier,
            @InputArgument String name,
            @InputArgument String description
    ) throws NotFoundException {
        WorkspaceDTO workspaceDTO = WorkspaceDTO.builder()
                .id(id)
                .departmentId(departmentId)
                .identifier(identifier)
                .name(name)
                .description(description)
                .build();

        return this.workspaceMutationProxy.updateWorkspace(workspaceDTO);
    }

    @DgsData(field = "remove", parentType = "workspace_mutation")
    public Boolean remove(
            @InputArgument List<Long> ids
    ) throws NotFoundException {
        ListIdsDTO listIdsDTO = new ListIdsDTO(ids);
        return this.workspaceMutationProxy.remove(listIdsDTO);
    }

}
