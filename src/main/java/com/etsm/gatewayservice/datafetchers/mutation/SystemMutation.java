package com.etsm.gatewayservice.datafetchers.mutation;

import DTO.Credentials;
import DTO.EmployeeDTO;
import DTO.LoginDTO;
import com.etsm.gatewayservice.interfaces.authService.AuthServiceProxy;
import com.etsm.gatewayservice.interfaces.employeeService.mutation.EmployeeMutationProxy;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.web.bind.annotation.RequestHeader;

/**
  mutation{
    system{
      initialize(login: "login", password: "password", firstName: "Anton", secondName: "Tam", email: "q@q.com"){
        id
        firstName
        secondName
        accessRoleSet{
          name
        }
      }
    }
  }
 */

@DgsComponent
public class SystemMutation {
    private final EmployeeMutationProxy employeeMutationProxy;
    private final AuthServiceProxy authServiceProxy;

    public SystemMutation(EmployeeMutationProxy employeeMutationProxy, AuthServiceProxy authServiceProxy) {
        this.employeeMutationProxy = employeeMutationProxy;
        this.authServiceProxy = authServiceProxy;
    }

    @DgsData(field = "system", parentType = "Mutation")
    public DataFetchingEnvironment systemWrapper(DataFetchingEnvironment dfe){
        return dfe;
    }

    @DgsData(field = "initialize", parentType = "system")
    public EmployeeDTO initSystem(@InputArgument String login,
                                  @InputArgument String password,
                                  @InputArgument String firstName,
                                  @InputArgument String secondName,
                                  @InputArgument String email) throws Exception {
        LoginDTO loginDTO = (LoginDTO) LoginDTO.builder()
                .password(password)
                .login(login)
                .firstName(firstName)
                .secondName(secondName)
                .email(email).build();

        return this.employeeMutationProxy.initialize(loginDTO);
    }

    @DgsData(field = "logon", parentType = "system")
    public String logon(@InputArgument String login, @InputArgument String password) throws Exception {
        LoginDTO loginDTO = LoginDTO.builder().login(login).password(password).build();
        Credentials credentials = this.authServiceProxy.logon(loginDTO);
        if(credentials != null){
            return credentials.getToken();
        }
        return null;
    }
}
