package com.etsm.gatewayservice.datafetchers.query;

import DTO.AccessRoleDTO;
import com.etsm.gatewayservice.interfaces.employeeService.query.AccessRoleQueryProxy;
import com.etsm.gatewayservice.services.securityChecker.SecurityChecker;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import exceptions.NotFoundException;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@DgsComponent
public class AccessRoleQuery {

    private final AccessRoleQueryProxy accessRoleQueryProxy;
    private final SecurityChecker securityChecker;

    public AccessRoleQuery(AccessRoleQueryProxy accessRoleQueryProxy, SecurityChecker securityChecker) {
        this.accessRoleQueryProxy = accessRoleQueryProxy;
        this.securityChecker = securityChecker;
    }

    @DgsData(field = "access_role", parentType = "Query")
    public DataFetchingEnvironment wrapper(DataFetchingEnvironment dfe, @RequestHeader String token) {
        return this.securityChecker.isAuthorized(dfe, token);
    }

    @DgsData(field = "access_roles", parentType = "access_role")
    public List<AccessRoleDTO> getAccessRoleList(){
        return this.accessRoleQueryProxy.getAccessRoleList();
    }

    @DgsData(field = "access_role", parentType = "access_role")
    public AccessRoleDTO getAccessRoleById(@InputArgument Long id) throws NotFoundException {
        return this.accessRoleQueryProxy.getAccessRoleById(id.toString());
    }

}
