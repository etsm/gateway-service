package com.etsm.gatewayservice.datafetchers.query;

import DTO.WorkspaceDTO;
import com.etsm.gatewayservice.interfaces.taskService.query.WorkspaceQueryProxy;
import com.etsm.gatewayservice.services.securityChecker.SecurityChecker;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import exceptions.NotFoundException;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@DgsComponent
public class WorkspaceQuery {
    private final WorkspaceQueryProxy workspaceQueryProxy;
    private final SecurityChecker securityChecker;

    public WorkspaceQuery(WorkspaceQueryProxy workspaceQueryProxy, SecurityChecker securityChecker) {
        this.workspaceQueryProxy = workspaceQueryProxy;
        this.securityChecker = securityChecker;
    }

    @DgsData(field = "workspace", parentType = "Query")
    public DataFetchingEnvironment wrapper(DataFetchingEnvironment dfe, @RequestHeader String token) {
        return this.securityChecker.isAuthorized(dfe, token);
    }

    @DgsData(field = "workspaces", parentType = "workspace")
    public List<WorkspaceDTO> getWorkspaceList(){
        return this.workspaceQueryProxy.getWorkspaces();
    }

    @DgsData(field = "workspace", parentType = "workspace")
    public WorkspaceDTO getWorkspaceById(@InputArgument Long id) throws NotFoundException {
        return this.workspaceQueryProxy.getWorkspaceById(id.toString());
    }
}
