package com.etsm.gatewayservice.datafetchers.query;

import DTO.DepartmentDTO;
import com.etsm.gatewayservice.interfaces.employeeService.query.DepartmentQueryProxy;
import com.etsm.gatewayservice.services.securityChecker.SecurityChecker;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import exceptions.NotFoundException;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@DgsComponent
public class DepartmentQuery {

    private final DepartmentQueryProxy departmentQueryProxy;
    private final SecurityChecker securityChecker;

    public DepartmentQuery(DepartmentQueryProxy departmentQueryProxy, SecurityChecker securityChecker) {
        this.departmentQueryProxy = departmentQueryProxy;
        this.securityChecker = securityChecker;
    }

    @DgsData(field = "department", parentType = "Query")
    public DataFetchingEnvironment wrapper(DataFetchingEnvironment dfe, @RequestHeader String token) {
        return this.securityChecker.isAuthorized(dfe, token);
    }

    @DgsData(field = "departments", parentType = "department")
    public List<DepartmentDTO> getDepartmentList(){
        return this.departmentQueryProxy.getDepartments();
    }

    @DgsData(field = "department", parentType = "department")
    public DepartmentDTO getDepartmentById(@InputArgument Long id) throws NotFoundException {
        return this.departmentQueryProxy.getDepartmentById(id.toString());
    }
}
