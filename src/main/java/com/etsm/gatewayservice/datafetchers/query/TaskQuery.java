package com.etsm.gatewayservice.datafetchers.query;

import DTO.TaskDTO;
import DTO.WorkspaceDTO;
import com.etsm.gatewayservice.interfaces.taskService.query.TaskQueryProxy;
import com.etsm.gatewayservice.services.securityChecker.SecurityChecker;
import com.netflix.graphql.dgs.DgsComponent;
import com.netflix.graphql.dgs.DgsData;
import com.netflix.graphql.dgs.InputArgument;
import exceptions.NotFoundException;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@DgsComponent
public class TaskQuery {
    private final TaskQueryProxy taskQueryProxy;
    private final SecurityChecker securityChecker;

    public TaskQuery(TaskQueryProxy taskQueryProxy, SecurityChecker securityChecker) {
        this.taskQueryProxy = taskQueryProxy;
        this.securityChecker = securityChecker;
    }

    @DgsData(field = "task", parentType = "Query")
    public DataFetchingEnvironment wrapper(DataFetchingEnvironment dfe, @RequestHeader String token) {
        return this.securityChecker.isAuthorized(dfe, token);
    }

    @DgsData(field = "task", parentType = "task")
    public TaskDTO getTaskByIdentifier(@InputArgument String identifier) throws NotFoundException {
        return this.taskQueryProxy.getTaskByIdentifier(identifier.toString());
    }
}
