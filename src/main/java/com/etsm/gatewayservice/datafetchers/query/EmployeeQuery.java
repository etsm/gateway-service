package com.etsm.gatewayservice.datafetchers.query;

import DTO.EmployeeDTO;
import com.etsm.gatewayservice.interfaces.employeeService.query.EmployeeQueryProxy;
import com.etsm.gatewayservice.services.securityChecker.SecurityChecker;
import com.netflix.graphql.dgs.*;
import exceptions.NotFoundException;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

@DgsComponent
@Component
public class EmployeeQuery {

    private final EmployeeQueryProxy employeeQueryProxy;
    private final SecurityChecker securityChecker;

    public EmployeeQuery(EmployeeQueryProxy employeeQueryProxy, SecurityChecker securityChecker) {
        this.employeeQueryProxy = employeeQueryProxy;
        this.securityChecker = securityChecker;
    }

    @DgsData(field = "employee", parentType = "Query")
    public DataFetchingEnvironment wrapper(DataFetchingEnvironment dfe, @RequestHeader String token) {
        return this.securityChecker.isAuthorized(dfe, token);
    }

    @DgsData(field = "employee", parentType = "employee")
    public EmployeeDTO getEmployee(@InputArgument Long id, @RequestHeader String token) throws NotFoundException {
        return this.employeeQueryProxy.getEmployeeById(id.toString());
    }

    @DgsData(field = "employees", parentType = "employee")
    public List<EmployeeDTO> getEmployeeList(){
        return this.employeeQueryProxy.getAllEmployees();
    }
}
