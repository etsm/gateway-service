package com.etsm.gatewayservice.interfaces.employeeService.mutation;

import DTO.AccessRoleDTO;
import DTO.ListIdsDTO;
import com.etsm.gatewayservice.configuration.CoreFeignConfiguration;
import exceptions.NotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "employee-service", contextId = "accessRoleMutation", configuration = CoreFeignConfiguration.class)
@RequestMapping("/mutation/access_role")
public interface AccessRoleMutationProxy {
    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    AccessRoleDTO createAccessRole(@RequestBody AccessRoleDTO accessRoleDTO);

    @DeleteMapping("/remove")
    Boolean remove(@RequestBody ListIdsDTO ids);

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    AccessRoleDTO updateAccessRole(@RequestBody AccessRoleDTO accessRoleDTO) throws NotFoundException;
}
