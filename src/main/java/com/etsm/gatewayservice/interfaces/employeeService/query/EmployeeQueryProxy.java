package com.etsm.gatewayservice.interfaces.employeeService.query;

import DTO.AccessRoleDTO;
import DTO.DepartmentDTO;
import DTO.EmployeeDTO;
import com.etsm.gatewayservice.configuration.CoreFeignConfiguration;
import exceptions.NotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Set;

@FeignClient(name = "employee-service", contextId = "employeeQuery", configuration = CoreFeignConfiguration.class)
@RequestMapping("/query/employee")
public interface EmployeeQueryProxy {
    @GetMapping("/all")
    List<EmployeeDTO> getAllEmployees();

    @GetMapping("/{id}")
    EmployeeDTO getEmployeeById(@PathVariable String id) throws NotFoundException;

    @GetMapping("/{employeeId}/accessRoles")
    Set<AccessRoleDTO> getEmployeeAccessRoles(@PathVariable String employeeId ) throws NotFoundException;

    @GetMapping("/{employeeId}/department")
    DepartmentDTO getEmployeeDepartment(@PathVariable String employeeId ) throws NotFoundException;
}
