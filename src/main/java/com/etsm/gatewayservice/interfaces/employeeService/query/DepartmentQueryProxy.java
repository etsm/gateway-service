package com.etsm.gatewayservice.interfaces.employeeService.query;

import DTO.DepartmentDTO;
import DTO.EmployeeDTO;
import com.etsm.gatewayservice.configuration.CoreFeignConfiguration;
import exceptions.NotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(name = "employee-service", contextId = "departmentQuery", configuration = CoreFeignConfiguration.class)
@RequestMapping("/query/department")
public interface DepartmentQueryProxy {
    @GetMapping("/all")
    List<DepartmentDTO> getDepartments();

    @GetMapping("/{id}")
    DepartmentDTO getDepartmentById(@PathVariable String id) throws NotFoundException;

    @GetMapping("/headOfDepartment/{departmentId}")
    EmployeeDTO getHeadOfDepartment(@PathVariable String departmentId) throws NotFoundException;
}
