package com.etsm.gatewayservice.interfaces.employeeService.query;

import DTO.AccessRoleDTO;
import com.etsm.gatewayservice.configuration.CoreFeignConfiguration;
import exceptions.NotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(name = "employee-service", contextId = "accessRoleQuery", configuration = CoreFeignConfiguration.class)
@RequestMapping("/query/access-role")
public interface AccessRoleQueryProxy {

    @GetMapping("/all")
    List<AccessRoleDTO> getAccessRoleList();

    @GetMapping("/{id}")
    AccessRoleDTO getAccessRoleById(@PathVariable String id) throws NotFoundException;
}
