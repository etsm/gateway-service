package com.etsm.gatewayservice.interfaces.employeeService.mutation;

import DTO.ComplexPasswordDTO;
import DTO.EmployeeDTO;
import DTO.ListIdsDTO;
import DTO.LoginDTO;
import com.etsm.gatewayservice.configuration.CoreFeignConfiguration;
import exceptions.NotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "employee-service", contextId = "employeeMutation", configuration = CoreFeignConfiguration.class)
@RequestMapping("/mutation/employee")
public interface EmployeeMutationProxy {

    @PostMapping(value = "/initialize", consumes = MediaType.APPLICATION_JSON_VALUE)
    EmployeeDTO initialize(@RequestBody LoginDTO loginDto) throws Exception ;

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    EmployeeDTO updateEmployeeById(@RequestBody EmployeeDTO employeeDTO) throws NotFoundException ;

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    EmployeeDTO createEmployee(@RequestBody LoginDTO employeeDTO) throws Exception;

    @DeleteMapping(value = "/remove")
    Boolean remove(@RequestBody ListIdsDTO ids);
}
