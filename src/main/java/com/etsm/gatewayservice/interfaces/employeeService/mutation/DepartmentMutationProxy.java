package com.etsm.gatewayservice.interfaces.employeeService.mutation;

import DTO.DepartmentDTO;
import DTO.ListIdsDTO;
import com.etsm.gatewayservice.configuration.CoreFeignConfiguration;
import exceptions.NotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "employee-service", contextId = "departmentMutation", configuration = CoreFeignConfiguration.class)
@RequestMapping("/mutation/department")
public interface DepartmentMutationProxy {
    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    DepartmentDTO createDepartment(@RequestBody DepartmentDTO departmentDTO) throws NotFoundException;

    @DeleteMapping("/remove")
    Boolean remove(@RequestBody ListIdsDTO ids);

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    DepartmentDTO updateDepartment(@RequestBody DepartmentDTO departmentDTO) throws NotFoundException;
}
