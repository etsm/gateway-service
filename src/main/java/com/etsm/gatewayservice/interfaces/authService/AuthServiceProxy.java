package com.etsm.gatewayservice.interfaces.authService;

import DTO.Credentials;
import DTO.ListIdsDTO;
import DTO.LoginDTO;
import com.etsm.gatewayservice.configuration.CoreFeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@FeignClient(name = "auth-service", contextId = "authService", configuration = CoreFeignConfiguration.class)
@RequestMapping("/")
public interface AuthServiceProxy {
    @PostMapping("/user/add")
    Long addUser(@RequestBody LoginDTO loginDTO) throws Exception;

    @DeleteMapping("/user/remove")
    Boolean remove(@RequestBody ListIdsDTO ids);

    @GetMapping("/user/loginFree/{login}")
    Boolean isNotExistLogin(@PathVariable String login);

    @GetMapping("/user/existUser/{id}")
    Boolean isExistUser(@PathVariable String id);

    @PostMapping("/user/logon")
    Credentials logon(@RequestBody LoginDTO loginDTO) throws Exception ;

    @PostMapping("/user/logout")
    Boolean logout(Credentials credentials);

    @PostMapping("/token/verify")
    Map<String, Object> verifyToken(@RequestBody Credentials credentials);

    @PutMapping("/user/update/{id}/{newLogin}")
    LoginDTO updateLogin(@PathVariable String id, @PathVariable String newLogin) throws Exception;
}
