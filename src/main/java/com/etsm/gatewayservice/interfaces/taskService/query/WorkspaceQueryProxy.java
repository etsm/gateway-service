package com.etsm.gatewayservice.interfaces.taskService.query;

import DTO.TaskDTO;
import DTO.WorkspaceDTO;
import com.etsm.gatewayservice.configuration.CoreFeignConfiguration;
import exceptions.NotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/query/workspace")
@FeignClient(name = "task-service",  contextId = "workspaceQuery", configuration = CoreFeignConfiguration.class)
public interface WorkspaceQueryProxy {
    @GetMapping("/all")
    List<WorkspaceDTO> getWorkspaces();

    @GetMapping("/{id}")
    WorkspaceDTO getWorkspaceById(@PathVariable String id) throws NotFoundException;

    @GetMapping("/{id}/taskList")
    List<TaskDTO> getTaskListByWorkspace(@PathVariable String id) throws NotFoundException;

    @GetMapping("/byDepartmentId/{departmentId}")
    List<WorkspaceDTO> getWorkspacesByDepartmentId(@PathVariable String departmentId);
}