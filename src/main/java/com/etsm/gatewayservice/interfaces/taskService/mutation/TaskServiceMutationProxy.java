package com.etsm.gatewayservice.interfaces.taskService.mutation;

import DTO.EmployeeDTO;
import DTO.ListIdsDTO;
import DTO.TaskDTO;
import com.etsm.gatewayservice.configuration.CoreFeignConfiguration;
import exceptions.NotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "task-service", contextId = "taskMutation", configuration = CoreFeignConfiguration.class)
@RequestMapping("/mutation/task")
public interface TaskServiceMutationProxy {
    @PostMapping(value = "/create", consumes = {MediaType.APPLICATION_JSON_VALUE})
    TaskDTO createTask(@RequestBody TaskDTO taskDTO) throws Exception;

    @PutMapping(value = "/update", consumes = {MediaType.APPLICATION_JSON_VALUE})
    TaskDTO updateTask(@RequestBody TaskDTO taskDTO) throws NotFoundException ;

    @PutMapping(value = "/update-blocks-list/{targetId}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    TaskDTO updateBlocksListTask(@PathVariable String targetId, @RequestBody ListIdsDTO ids) throws Exception;

    @PutMapping(value = "/update-blocked-list/{targetId}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    TaskDTO updateBlockedListTask(@PathVariable String targetId, @RequestBody ListIdsDTO ids) throws Exception;

    @DeleteMapping("/remove")
    Boolean remove(@RequestBody ListIdsDTO idList);

    @PutMapping(value = "/employee-was-deleted", consumes = MediaType.APPLICATION_JSON_VALUE)
    Boolean synchronize(@RequestBody List<EmployeeDTO> employeeDTO);
}
