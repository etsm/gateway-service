package com.etsm.gatewayservice.interfaces.taskService.mutation;

import DTO.DepartmentDTO;
import DTO.ListIdsDTO;
import DTO.WorkspaceDTO;
import com.etsm.gatewayservice.configuration.CoreFeignConfiguration;
import exceptions.NotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/mutation/workspace")
@FeignClient(name = "task-service", contextId = "workspaceMutation", configuration = CoreFeignConfiguration.class)
public interface WorkspaceMutationProxy {
    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    WorkspaceDTO createWorkspace(@RequestBody WorkspaceDTO workspaceDTO);

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    WorkspaceDTO updateWorkspace(@RequestBody WorkspaceDTO workspaceDTO) throws NotFoundException;

    @DeleteMapping(value = "/remove")
    Boolean remove(@RequestBody ListIdsDTO listIds);

    @PutMapping(value = "/department-was-deleted", consumes = MediaType.APPLICATION_JSON_VALUE)
    Boolean synchronize(@RequestBody DepartmentDTO departmentDTO);
}
