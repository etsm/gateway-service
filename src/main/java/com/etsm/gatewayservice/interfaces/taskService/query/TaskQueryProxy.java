package com.etsm.gatewayservice.interfaces.taskService.query;

import DTO.TaskDTO;
import com.etsm.gatewayservice.configuration.CoreFeignConfiguration;
import exceptions.NotFoundException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(name = "task-service", contextId = "taskQuery", configuration = CoreFeignConfiguration.class)
@RequestMapping("/query/task")
public interface TaskQueryProxy {
    @GetMapping("/allBy/author/{id}")
    List<TaskDTO> getTaskListByAuthorId(@PathVariable String id);

    @GetMapping("/allBy/executor/{id}")
    List<TaskDTO> getTaskListByExecutorId(@PathVariable String id);

    @GetMapping("/{id}")
    TaskDTO getTask(@PathVariable String id) throws NotFoundException;

    @GetMapping("/all")
    List<TaskDTO> getTaskList();

    @GetMapping("/blocks-list/{taskId}")
    List<TaskDTO> getBlocksList(@PathVariable String taskId) throws NotFoundException;

    @GetMapping("/blocked-list/{taskId}")
    List<TaskDTO> getBlockedList(@PathVariable String taskId) throws NotFoundException;

    @GetMapping("/byIdentifier/{identifier}")
    TaskDTO getTaskByIdentifier(@PathVariable String identifier) throws NotFoundException;
}
