package com.etsm.gatewayservice.services.securityChecker;

import graphql.schema.DataFetchingEnvironment;
import org.springframework.dao.PermissionDeniedDataAccessException;

public interface SecurityChecker {
    DataFetchingEnvironment isAuthorized(DataFetchingEnvironment dfe,  String token) throws PermissionDeniedDataAccessException;
}
