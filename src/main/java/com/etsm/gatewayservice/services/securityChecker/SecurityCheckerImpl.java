package com.etsm.gatewayservice.services.securityChecker;

import DTO.Credentials;
import com.etsm.gatewayservice.interfaces.authService.AuthServiceProxy;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Service;

@Service
public class SecurityCheckerImpl implements SecurityChecker{

    private final AuthServiceProxy authServiceProxy;

    public SecurityCheckerImpl(AuthServiceProxy authServiceProxy) {
        this.authServiceProxy = authServiceProxy;
    }


    @Override
    public DataFetchingEnvironment isAuthorized(DataFetchingEnvironment dfe, String token) throws PermissionDeniedDataAccessException {
        Credentials credentials = new Credentials();
        credentials.setToken(token);
        if(this.authServiceProxy.verifyToken(credentials) != null){
            return dfe;
        }

        throw new PermissionDeniedDataAccessException("Logon", new Exception());
    }
}
