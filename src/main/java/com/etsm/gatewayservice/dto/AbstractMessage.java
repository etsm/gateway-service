package com.etsm.gatewayservice.dto;

import lombok.Getter;
import lombok.Setter;

public class AbstractMessage<T extends Object> {
    @Getter @Setter private final String key;
    @Getter @Setter private final T payload;

    public AbstractMessage(String key, T payload) {
        this.key = key;
        this.payload = payload;
    }


}
